minetest.register_chatcommand("repb", modRepairer.getPropCom_RepBAdd())
minetest.register_chatcommand("replaceblock", modRepairer.getPropCom_RepBAdd())
minetest.register_chatcommand("repairblock", modRepairer.getPropCom_RepBAdd())

minetest.register_chatcommand("stoprb", modRepairer.getPropCom_RepBDel())
minetest.register_chatcommand("stoprepb", modRepairer.getPropCom_RepBDel())
minetest.register_chatcommand("stopreplaceblock", modRepairer.getPropCom_RepBDel())
minetest.register_chatcommand("stoprepairblock", modRepairer.getPropCom_RepBDel())

minetest.register_chatcommand("rbl", modRepairer.getPropCom_RepBList())
minetest.register_chatcommand("rblist", modRepairer.getPropCom_RepBList())
minetest.register_chatcommand("repblocklist", modRepairer.getPropCom_RepBList())
minetest.register_chatcommand("repairers", modRepairer.getPropCom_RepBList())

