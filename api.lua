modRepairer.debug = function(playername, functionname, message)
	--minetest.chat_send_all("modRepairer.isdebuged() = "..dump(modRepairer.isdebuged()))
	if modRepairer.isdebuged() then
		local header = ""
		if type(functionname)=="string" and functionname:trim()~="" then
			header = "[REPAIRER:DEBUG:"..functionname:upper().."] "
		else
			header = "[REPAIRER:DEBUG] "
		end
		if type(playername)=="string" and playername:trim()~="" then
			minetest.chat_send_player(playername, core.colorize("#FFFF00", header)..message)
		else
			minetest.chat_send_all(core.colorize("#FFFF00", header)..message)
			minetest.log("info", header..message)
		end
	end
end

modRepairer.doPrint = function(message, playername)
	if type(modRepairer.message)=="nil" or modRepairer.message ~= message then --Evita que a message no chat seja repetida!!!!
		modRepairer.message = message
		if type(playername)=="string" and playername:trim()~="" then
			minetest.chat_send_player(playername, message)
		elseif modRepairer.isPrintInPublicChat then
			--if minetest.setting_getbool("log_mods") then minetest.chat_send_all(message)	end
			minetest.chat_send_all(message)
		end
	end
	minetest.log("action", core.strip_colors(message)) --so grava a message se o arquivo 'minetest.conf' permitir.
	--print(message)--Imprime no terminal de controle independente se é ou nao gravado o Log ou avisado ao jogador sobre o ocorrido.
end

modRepairer.getCountReplaces = function()
	return table.getn(modRepairer.replace.nodes)
end

modRepairer.getNodeReplaces = function()
	if type(modRepairer.replace)~="table" then 
		modRepairer.replace = { } 
	end
	if type(modRepairer.replace.nodes)~="table" then 
		modRepairer.replace.nodes = { } 
	end
	return modRepairer.replace.nodes
end

modRepairer.setNodeReplaces = function(replaces)
	if type(modRepairer.replace)~="table" then 
		modRepairer.replace = { } 
	end
	if type(modRepairer.replace.nodes)~="table" then 
		modRepairer.replace.nodes = { } 
	end
	if type(replaces)=="table" then
		modRepairer.replace.nodes = replaces
		return true, "ok"
	else
		return false, "The 'replaces' parameter is not in table format!"
	end	
end


modRepairer.doSave = function()
	local file = io.open(modRepairer.urlTabela, "w")
	if file then
		file:write(minetest.serialize(modRepairer.replace))
		file:close()
	end
end

modRepairer.doLoad = function()
	local file = io.open(modRepairer.urlTabela, "r")
	if file then
		modRepairer.replace = minetest.deserialize(file:read("*all"))
		file:close()
		if type(modRepairer.replace) ~= "table" then
			minetest.log('erro',
				"[REPAIRER:ERRO] "
				..modRepairer.translate("The file '%s' is not in table format!"):format(modRepairer.urlTabela)
			)
			return
		end
		if not modRepairer.replace.nodes then modRepairer.replace.nodes = {} end
		if not modRepairer.replace.entities then modRepairer.replace.entities = {} end
	end
end

modRepairer.doFindNodes = function()
	--DELETAR ABM
	local newAbmList = { }
	for i, abm in ipairs(minetest.registered_abms) do
		if abm.label ~= "repairer" then
			table.insert(newAbmList, abm)
		end
	end
	minetest.registered_abms = newAbmList

	if modRepairer.getCountReplaces() >=1 then
		modRepairer.oldBlocks = {}
		modRepairer.newBlocks = {}
		for _,replace in ipairs(modRepairer.getNodeReplaces()) do
			table.insert(modRepairer.oldBlocks, replace[1])
			modRepairer.newBlocks[replace[1]:trim()] = replace[2]:trim()
		end
		
		--RECRIAR ABM
		
		if table.getn(modRepairer.oldBlocks)>=1 then
			minetest.register_abm({
				label = "repairer",
				nodenames = modRepairer.oldBlocks,
				interval = 1,
				chance = 1,
				action = function(pos, node)
					local old = node.name:trim()
					if type(old)=="string" 
						and old~=""
						and type(modRepairer.newBlocks[old])=="string"
						and modRepairer.newBlocks[old]:trim()~=""
					then
						local new = modRepairer.newBlocks[old]:trim()
						modRepairer.doPrint(
							core.colorize("#8888ff", "[REPAIRER]")..": "
							..modRepairer.translate("Replacing '%s' with '%s' in %s."):format(old, new, minetest.pos_to_string(pos)					)
						)
						local meta_old = minetest.get_meta(pos)
						minetest.env:set_node(pos, {name=new})
						local meta_new = minetest.get_meta(pos)
						meta_new = meta_old
					end
				end,
			})
		end--Final of: if table.getn(modRepairer.oldBlocks)>=1 then
	end --Final of: if modRepairer.getCountReplaces() >=1 then
end

modRepairer.doCommandReplaceBlock = function(playername, param)
	local block_old = nil
	local block_new = nil
	block_old, block_new = string.match(param, "^([^ ]+) +([^ ]+)$")
	if type(block_old)~="string" or block_old:trim() == "" then
		block_old = string.match(param, "^([^ ]+)$")
		block_new = "air"
	end

	if type(block_old)=="string" and block_old:trim() ~= "" and type(block_new)=="string" and block_new:trim() ~= "" then
	
		if not minetest.registered_items[block_new] then
			modRepairer.doPrint(
				core.colorize("#ff0000", "[REPAIRER:ERRO]")..": "
				..modRepairer.translate("The New-Block '%s' does not exist, or has not been previously registered."):format(block_new)
			,playername)
			return true
		end

		for _, old in ipairs(modRepairer.getNodeReplaces()) do
			if old[1]:trim() == block_old:trim() then
				modRepairer.doPrint(
					core.colorize("#ff0000", "[REPAIRER:ERRO]")..": "
					..modRepairer.translate("The Old-Block '%s' was already registered in the cleaning list."):format(block_old:trim())
				,playername)
				return true
			end
		end
		
		--modRepairer.replace.nodes[#modRepairer.replace.nodes + 1] = { block_old:trim(), block_new:trim() }
		local nodes = modRepairer.getNodeReplaces()
		table.insert(nodes, { block_old:trim(), block_new:trim() })
		modRepairer.setNodeReplaces(nodes)
		
		modRepairer.doSave()
		modRepairer.doPrint(
			core.colorize("#00ff00", "[REPAIRER:OK]")..": "
			..modRepairer.translate("The Old-Block '%s' will be replaced with '%s'!"):format(block_old, block_new)
		,playername)
		modRepairer.doFindNodes()
		return true
	else
		modRepairer.doPrint(
			core.colorize("#ff0000", "[REPAIRER:ERRO]").." /repb <"..modRepairer.translate("OldBlock").."> [<"..modRepairer.translate("NewBlock")..">]"
			..core.colorize("#888888", 
				"\n\t* "..modRepairer.translate("Replaces a disabled mod block with an active mod block.")
				.."\n\t* "..modRepairer.translate(
					"The parameter [<%s>] is not mandatory. But if it is not declared, [<%s>] will be automatically replaced by air."
				):format("NewBlock", "NewBlock")
			)
		,playername)
		return true
	end
end

modRepairer.doDelReplace = function(playername, from_node)
	if modRepairer.getCountReplaces() >= 1 then
		local newNodeList = { }
		local newNodeCount = 0
		for _, myReplace in ipairs(modRepairer.getNodeReplaces()) do
			if myReplace[1] ~= from_node:trim() then
				table.insert(newNodeList, myReplace)
				newNodeCount = newNodeCount + 1
			end
		end
		--modRepairer.replace.nodes = newNodeList
		modRepairer.setNodeReplaces(newNodeList)
		modRepairer.doSave()
		modRepairer.doFindNodes()
		return true, modRepairer.translate("Nodes '%s' removed from repair list!"):format(from_node)
	else
		return false, modRepairer.translate("There are no registered blocks to be removed!")
	end
end


modRepairer.showFormSpecReplaces = function(playername, selLine)
	if type(playername)=="string" and playername~="" then
		local player = minetest.get_player_by_name(playername)
		if type(player)~="nil" and player:is_player() then
			if type(selLine)~="number" 
			   or (
			      type(selLine)=="number" 
			      and selLine < 0
		      ) 
		   then
				selLine = 0
		   end
		   if selLine > modRepairer.getCountReplaces() + 1 then
		      selLine = modRepairer.getCountReplaces() + 1
		   end
			local poliline = "#FFFF00"
				..", "..minetest.formspec_escape(modRepairer.translate("N°")).." "
				..", "..minetest.formspec_escape(modRepairer.translate("FROM NODE")).." "
				..", "..minetest.formspec_escape(modRepairer.translate("TO NODE")).." "
			if modRepairer.getCountReplaces() >= 1 then
				for i, myReplace in ipairs(modRepairer.getNodeReplaces()) do
					local line = "#FFF"
						..", "..minetest.formspec_escape(("%02d°"):format(i)).." "
						..", "..minetest.formspec_escape(myReplace[1]).." "
						..", "..minetest.formspec_escape(myReplace[2]).." " --COR, COL1, COL2, COLN
					poliline = poliline..","..line
				end
			end


			local formspec = "size[16,10]"
				.."bgcolor[#636D7644;false]"
				--.."bgcolor[#636D76FF;false]"
				--..default.gui_bg
				--..default.gui_bg_img
				--..default.gui_slots
				
				--.."bgcolor[#636D76FF;false]"
				--.."background[-0.25,-0.25;10,11;safe_inside.png]"
				--.."button[0,0.0;4,0.5;btnAtmMain;"..minetest.formspec_escape(core.colorize("#FFFFFF", modRepairer.translate("BACK"))).."]"
				--.."button_exit[0,3.0;4,0.5;;"..minetest.formspec_escape(core.colorize("#FFFFFF", modRepairer.translate("EXIT"))).."]"
				
				.."           box[0.00,0.10;15.5,0.75;#000088]"
				.."         label[0.50,0.25;"..minetest.formspec_escape(core.colorize("#FFFFFF", modMinerTrade.translate("REPLACE NODE LIST"))).."]"
				--..      "button[0.25,0.50;5.00,0.5;btnAtmMain;"..minetest.formspec_escape(core.colorize("#FFFFFF", modRepairer.translate("BACK"))).."]"
				.."           box[0.00,1.25;15.5,8.5;#00000088]"
				--..       "style[fldStatement;bgcolor=red;textcolor=yellow;border=true]" --ISSO FUNCIONA OK.
				.."  tableoptions[color=#FFFFFF;background=#00004488;border=true;highlight=#8888FF;highlight_text=#000000]" --ISSO TAMBÉM FUNCIONA OK.
				.."  tablecolumns[color;text;text;text]" --COR, COL1, COL2, COLN
				.."         table[0.00,1.25;15.0,6.75;tblReplaces;"..poliline..";"..selLine.."]"

				--.."        button[6.50,9.00;3.00,0.5;btnAddReplace;"..minetest.formspec_escape(core.colorize("#FFFFFF", modRepairer.translate("ADD"))).."]"
				.."        button[9.50,9.00;3.00,0.5;btnDelReplace;"..minetest.formspec_escape(core.colorize("#FFFFFF", modRepairer.translate("DELETE"))).."]"
				.."   button_exit[12.50,9.00;3.00,0.5;;"..minetest.formspec_escape(core.colorize("#FFFFFF", modRepairer.translate("EXIT"))).."]"
			
			minetest.show_formspec(
			   playername, 
			   "frmReplaces", 
			   formspec
		   )
		end --Final of: if type(player)~="nil" and player:is_player() then
	end --Final of: if type(sendername)=="string" and sendername~="" then
end

modRepairer.getPropCom_RepBList = function()
	return  {
		--params = "<"..modRepairer.translate("OldBlock").."> [<"..modRepairer.translate("NewBlock")..">]",
		description = core.colorize("#888888", modRepairer.translate("Displays the list of replacements that will be applied after the Server restarts.")),
		privs = {server=true},
		func = function(playername)
			modRepairer.showFormSpecReplaces(playername, 0)
			return true
		end,
	}
end

modRepairer.getPropCom_RepBAdd = function()
	return  {
		params = "<"..modRepairer.translate("OldBlock").."> [<"..modRepairer.translate("NewBlock")..">]",
		description = core.colorize("#888888", modRepairer.translate("Adds a 'replacement' to the list of replacements that will be applied after the Server restarts.")),
		privs = {server=true},
		func = function(playername, param)
			return modRepairer.doCommandReplaceBlock(playername, param)
		end,
	}
end

modRepairer.getPropCom_RepBDel = function()
	return  {
		params = "<"..modRepairer.translate("OldBlock")..">",
		description = core.colorize("#888888", modRepairer.translate("Remove a 'replacement' to the list of replacements that will be applied after the Server restarts.")),
		privs = {server=true},
		func = function(playername, from_node)
			local result, bycause = modRepairer.doDelReplace(playername, from_node)
			local colortext = "#00FF00"
			if not result then 
				colortext = "#FF0000" 
			end
			modRepairer.doPrint(
				core.colorize(colortext, "[REPAIRER]").." "..bycause
				, playername
			)
			return result
		end,
	}
end

minetest.register_on_player_receive_fields(function(sender, formname, fields)
	local sendername = sender:get_player_name()
	if type(formname)=="string" then
		if formname == "frmReplaces" then
			--modRepairer.debug(sendername, "minetest.register_on_player_receive_fields", "fields = "..dump(fields))
			--[[ ]]
			if fields.tblReplaces then
				if type(modRepairer.selReplece)=="nil" then
					modRepairer.selReplece = { }
				end
				local tblReplaces = minetest.explode_table_event(fields.tblReplaces)
				if type(tblReplaces) == "table" and type(tblReplaces.row) == "number" and tblReplaces.row >= 2 and tblReplaces.row <= modRepairer.getCountReplaces()+1 then
					modRepairer.selReplece[sendername] = tblReplaces.row
					return true
				end				
			elseif fields.btnDelReplace then
				if type(modRepairer.selReplece)=="table" 
					and type(modRepairer.selReplece[sendername])=="number"
					and modRepairer.selReplece[sendername] >= 2
					and modRepairer.selReplece[sendername] <= modRepairer.getCountReplaces() + 1
				then
					--modRepairer.debug(sendername, "minetest.register_on_player_receive_fields", "modRepairer.selReplece[sendername] = "..dump(modRepairer.selReplece[sendername]))
					local selNode = modRepairer.selReplece[sendername]
					local replaces = modRepairer.getNodeReplaces()
					table.remove(replaces, selNode - 1)
					--modRepairer.debug(sendername, "minetest.register_on_player_receive_fields", "replaces = "..dump(replaces))
					--[[ ]]
					local result, because = modRepairer.setNodeReplaces(replaces)
					modRepairer.doSave()
					modRepairer.doFindNodes()
					--[[  ]]
					local colortext = "#00FF00"
					local titletext = "[REPAIRER]"
					if not result then 
						colortext = "#FF0000" 
						titletext = "[REPAIRER:ERROR]"
					else
						because =  modRepairer.translate("The repairer was successfully deleted!")
					end
					modRepairer.doPrint(
						core.colorize(colortext, titletext).." "..because
						, sendername
					)
					--[[  ]]
					--modRepairer.debug(sendername, "receive_fields", "sendername = "..dump(sendername))
					--modRepairer.debug(sendername, "receive_fields", "selNode = "..dump(selNode))
					modRepairer.showFormSpecReplaces(sendername, selNode)
					--modRepairer.showFormSpecReplaces(sendername, 0)
					--[[  ]]
					return true
				end
			end
			--[[  ]]
		end
	end
end)
