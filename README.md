# Repairer 

[Minetest Mod] Replaces "Unknow Node" with another Node chosen by the server administrator.

![screenshot]

### LICENSE:

* [GNU-GPL-3.0-or-later]

### STABLE DOWNLOAD:

* [https://gitlab.com/lunovox/repairer/-/tags](https://gitlab.com/lunovox/repairer/-/tags)

### REPOSITORY:

* [https://gitlab.com/lunovox/repairer](https://gitlab.com/lunovox/repairer)


### DEPENDENCIES:

* **Mandatory:** 
	* [default] (Internal to Minetest Game)
	* [screwdriver] (Internal to Minetest Game)

* **Optional:** 
	* [intllib] (Internationalization library for Minetest.)

### DEVELOPERS:

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](https://qoto.org/@lunovox), [WebChat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [audio conference](mumble:mumble.disroot.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)
	
### COMMANDS:

* Displays the list of replacements that will be applied after the Server restarts.
   * ````/rbl````
   * ````/rblist````
   * ````/repblocklist````
   * ````/repairers````
   
* Adds a 'replacement' to the list of replacements that will be applied after the Server restarts.
	* ````/repb <OldNode> [<NewNode>]````
	* ````/replaceblock <OldNode> [<NewNode>]````
	* ````/repairblock <OldNode> [<NewNode>]````

* Remove a 'replacement' to the list of replacements that will be applied after the Server restarts.
	* ````/stoprb <OldNode>````
	* ````/stoprepb <OldNode>````
	* ````/stopreplaceblock <OldNode>````
	* ````/stoprepairblock <OldNode>````

### SETTINGS:

These are the settings saved in the '````minetest.conf````' file.

* ````repairer.debug = <true/false>```` ← Print Debug Info in Chat Terminal. Default: false
* ````repairer.printInPublicChat = <true/false>```` ← Print map repairs in public chat. Default: true

[screenshot]:https://gitlab.com/lunovox/repairer/-/raw/master/screenshot.png
[GNU-GPL-3.0-or-later]:https://gitlab.com/lunovox/repairer/-/raw/master/LICENSE
[default]:https://github.com/minetest/minetest_game/
[screwdriver]:https://github.com/minetest/minetest_game/
[intllib]:https://github.com/minetest-mods/intllib
[lunovox@disroot.org]:mailto:lunovox@disroot.org
[libreplanet.org#User:Lunovox]:https://libreplanet.org/wiki/User:Lunovox
