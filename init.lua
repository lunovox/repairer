modRepairer = {
	modname = minetest.get_current_modname(),
	modpath = minetest.get_modpath(minetest.get_current_modname()),
	urlTabela = minetest.get_worldpath().."/repairer.tbl",
	isPrintInPublicChat = (minetest.settings:get("repairer.printInPublicChat")~="false"),
	isdebuged = function() 
		return minetest.settings:get_bool("repairer.debug") 
	end,
	replace = {
		nodes = {},
	},
}

dofile(modRepairer.modpath.."/translate.lua")
dofile(modRepairer.modpath.."/api.lua")
dofile(modRepairer.modpath.."/commands.lua")
dofile(modRepairer.modpath.."/repairer_bench.lua")

modRepairer.doLoad()
modRepairer.doFindNodes()

minetest.log('action',"["..modRepairer.modname:upper().."] Carregado!")
